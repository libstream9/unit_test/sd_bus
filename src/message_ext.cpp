#include <stream9/sd_bus/message_ext.hpp>

#include "namespace.hpp"

#include <stream9/sd_bus/bus.hpp>
#include <stream9/sd_bus/message.hpp>

#include <vector>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(message_ext_)

    BOOST_AUTO_TEST_CASE(read_append_byte_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_byte(m, 100);

            m.seal(0);
            BOOST_TEST(m.signature() == "y");

            auto v = sdb::read_byte(m);

            BOOST_TEST(v == 100);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_boolean_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_boolean(m, true);

            m.seal(0);
            BOOST_TEST(m.signature() == "b");

            auto v = sdb::read_boolean(m);

            BOOST_TEST(v == true);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_int16_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_int16(m, 1234);

            m.seal(0);
            BOOST_TEST(m.signature() == "n");

            auto v = sdb::read_int16(m);

            BOOST_TEST(v == 1234);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_uint16_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_uint16(m, 1234);

            m.seal(0);
            BOOST_TEST(m.signature() == "q");

            auto v = sdb::read_uint16(m);

            BOOST_TEST(v == 1234);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_int32_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_int32(m, 1234);

            m.seal(0);
            BOOST_TEST(m.signature() == "i");

            auto v = sdb::read_int32(m);

            BOOST_TEST(v == 1234);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_uint32_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_uint32(m, 1234);

            m.seal(0);
            BOOST_TEST(m.signature() == "u");

            auto v = sdb::read_uint32(m);

            BOOST_TEST(v == 1234);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_int64_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_int64(m, 1234);

            m.seal(0);
            BOOST_TEST(m.signature() == "x");

            auto v = sdb::read_int64(m);

            BOOST_TEST(v == 1234);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_uint64_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_uint64(m, 1234);

            m.seal(0);
            BOOST_TEST(m.signature() == "t");

            auto v = sdb::read_uint64(m);

            BOOST_TEST(v == 1234);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_double_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_double(m, 3.14);

            m.seal(0);
            BOOST_TEST(m.signature() == "d");

            auto v = sdb::read_double(m);

            BOOST_TEST(v == 3.14);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_string_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_string(m, "foo");

            m.seal(0);
            BOOST_TEST(m.signature() == "s");

            auto v = sdb::read_string(m);

            BOOST_TEST(v == "foo");
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_object_path_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_object_path(m, "/org/foo");

            m.seal(0);
            BOOST_TEST(m.signature() == "o");

            auto v = sdb::read_object_path(m);

            BOOST_TEST(v == "/org/foo");
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_signature_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_signature(m, "a{us}");

            m.seal(0);
            BOOST_TEST(m.signature() == "g");

            auto v = sdb::read_signature(m);

            BOOST_TEST(v == "a{us}");
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_unix_fd_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_unix_fd(m, 1);

            m.seal(0);
            BOOST_TEST(m.signature() == "h");

            auto v = sdb::read_unix_fd(m);

            BOOST_TEST(v > 0);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_byte_array_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            uint8_t arr[] = { 1, 2, 3 };
            sdb::append_byte_array(m, arr);

            m.seal(0);
            BOOST_TEST(m.signature() == "ay");

            auto r = sdb::read_byte_array(m);

            BOOST_REQUIRE(r.size() == 3);
            BOOST_TEST(r[0] == 1);
            BOOST_TEST(r[1] == 2);
            BOOST_TEST(r[2] == 3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

#if 0
    BOOST_AUTO_TEST_CASE(read_append_boolean_array_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            int arr[] = { 1, 0, 3 };
            sdb::append_boolean_array(m, arr);

            m.seal(0);
            BOOST_TEST(m.signature() == "ab");

            auto r = sdb::read_boolean_array(m);

            BOOST_REQUIRE(r.size() == 3);
            BOOST_TEST(r[0] == 1);
            BOOST_TEST(r[1] == 0);
            BOOST_TEST(r[2] == 1);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }
#endif

    BOOST_AUTO_TEST_CASE(read_append_int16_array_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            int16_t arr[] = { 1, 2, 3 };
            sdb::append_int16_array(m, arr);

            m.seal(0);
            BOOST_TEST(m.signature() == "an");

            auto r = sdb::read_int16_array(m);

            BOOST_TEST_REQUIRE(r.size() == 3);
            BOOST_TEST(r[0] == 1);
            BOOST_TEST(r[1] == 2);
            BOOST_TEST(r[2] == 3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_uint16_array_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            uint16_t arr[] = { 1, 2, 3 };
            sdb::append_uint16_array(m, arr);

            m.seal(0);
            BOOST_TEST(m.signature() == "aq");

            auto r = sdb::read_uint16_array(m);

            BOOST_TEST_REQUIRE(r.size() == 3);
            BOOST_TEST(r[0] == 1);
            BOOST_TEST(r[1] == 2);
            BOOST_TEST(r[2] == 3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_int32_array_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            int32_t arr[] = { 1, 2, 3 };
            sdb::append_int32_array(m, arr);

            m.seal(0);
            BOOST_TEST(m.signature() == "ai");

            auto r = sdb::read_int32_array(m);

            BOOST_TEST_REQUIRE(r.size() == 3);
            BOOST_TEST(r[0] == 1);
            BOOST_TEST(r[1] == 2);
            BOOST_TEST(r[2] == 3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_uint32_array_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            uint32_t arr[] = { 1, 2, 3 };
            sdb::append_uint32_array(m, arr);

            m.seal(0);
            BOOST_TEST(m.signature() == "au");

            auto r = sdb::read_uint32_array(m);

            BOOST_TEST_REQUIRE(r.size() == 3);
            BOOST_TEST(r[0] == 1);
            BOOST_TEST(r[1] == 2);
            BOOST_TEST(r[2] == 3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_int64_array_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            int64_t arr[] = { 1, 2, 3 };
            sdb::append_int64_array(m, arr);

            m.seal(0);
            BOOST_TEST(m.signature() == "ax");

            auto r = sdb::read_int64_array(m);

            BOOST_TEST_REQUIRE(r.size() == 3);
            BOOST_TEST(r[0] == 1);
            BOOST_TEST(r[1] == 2);
            BOOST_TEST(r[2] == 3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_uint64_array_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            uint64_t arr[] = { 1, 2, 3 };
            sdb::append_uint64_array(m, arr);

            m.seal(0);
            BOOST_TEST(m.signature() == "at");

            auto r = sdb::read_uint64_array(m);

            BOOST_TEST_REQUIRE(r.size() == 3);
            BOOST_TEST(r[0] == 1);
            BOOST_TEST(r[1] == 2);
            BOOST_TEST(r[2] == 3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_append_double_array_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            double arr[] = { 1.1, 2.2, 3.3 };
            sdb::append_double_array(m, arr);

            m.seal(0);
            BOOST_TEST(m.signature() == "ad");

            auto r = sdb::read_double_array(m);

            BOOST_TEST_REQUIRE(r.size() == 3);
            BOOST_TEST(r[0] == 1.1);
            BOOST_TEST(r[1] == 2.2);
            BOOST_TEST(r[2] == 3.3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_array_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            m.append("a(yyy)", 2, 1, 2, 3, 4, 5, 6);
            m.seal(0);

            struct val {
                uint8_t v1;
                uint8_t v2;
                uint8_t v3;
            };

            std::vector<val> r;

            sdb::read_array(m, "(yyy)", [&] {
                while (!m.at_end()) {
                    sdb::read_struct(m, "yyy", [&]{
                        auto v1 = sdb::read_byte(m);
                        auto v2 = sdb::read_byte(m);
                        auto v3 = sdb::read_byte(m);

                        r.emplace_back(v1, v2, v3);
                    });
                }
            });

            BOOST_REQUIRE(r.size() == 2);
            BOOST_TEST(r[0].v1 == 1);
            BOOST_TEST(r[0].v2 == 2);
            BOOST_TEST(r[0].v3 == 3);
            BOOST_TEST(r[1].v1 == 4);
            BOOST_TEST(r[1].v2 == 5);
            BOOST_TEST(r[1].v3 == 6);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_struct_basic_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            m.append("(yy)", 1, 2);

            m.seal(0);

            sdb::read_struct(m, "yy", [&]{
                auto b1 = read_byte(m);
                auto b2 = read_byte(m);

                BOOST_TEST(b1 == 1);
                BOOST_TEST(b2 == 2);
            });
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_struct_complex_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            m.append("(y(yy))", 1, 2, 3);

            m.seal(0);

            int v1 {}, v2 {}, v3 {};

            sdb::read_struct(m, "y(yy)", [&]{
                v1 = sdb::read_byte(m);

                sdb::read_struct(m, "yy", [&]{
                    v2 = sdb::read_byte(m);
                    v3 = sdb::read_byte(m);
                });
            });

            BOOST_TEST(v1 == 1);
            BOOST_TEST(v2 == 2);
            BOOST_TEST(v3 == 3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(append_struct_basic_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_struct(m, "yy", [&] {
                sdb::append_byte(m, 1);
                sdb::append_byte(m, 2);
            });

            m.seal(0);
            BOOST_TEST(m.signature() == "(yy)");

            sdb::read_struct(m, "yy", [&]{
                auto b1 = sdb::read_byte(m);
                auto b2 = sdb::read_byte(m);

                BOOST_TEST(b1 == 1);
                BOOST_TEST(b2 == 2);
            });
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(append_struct_complex_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_struct(m, "yay", [&]{
                sdb::append_byte(m, 1);
                sdb::append_array(m, "y", [&]{
                    sdb::append_byte(m, 2);
                    sdb::append_byte(m, 3);
                });
            });

            m.seal(0);
            BOOST_TEST(m.signature() == "(yay)");

            sdb::read_struct(m, "yay", [&]{
                auto v1 = sdb::read_byte(m);
                BOOST_TEST(v1 == 1);

                auto arr = sdb::read_byte_array(m);
                BOOST_REQUIRE(arr.size() == 2);
                BOOST_TEST(arr[0] == 2);
                BOOST_TEST(arr[1] == 3);
            });
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_variant_basic_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            m.append("v", "y", 2);

            m.seal(0);
            BOOST_TEST(m.signature() == "v");

            sdb::read_variant(m, "y", [&]{
                auto b = sdb::read_byte(m);

                BOOST_TEST(b == 2);
            });
        }
        catch (...) {
            stream9::errors::print_error();
        }
    }

    BOOST_AUTO_TEST_CASE(read_variant_complex_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            m.append("v", "ay", 2, 1, 2);

            m.seal(0);
            BOOST_TEST(m.signature() == "v");

            std::vector<int> arr;

            sdb::read_variant(m, "ay", [&]{
                for (auto v: sdb::read_byte_array(m)) {
                    arr.push_back(v);
                }
            });

            BOOST_REQUIRE(arr.size() == 2);
            BOOST_TEST(arr[0] == 1);
            BOOST_TEST(arr[1] == 2);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(append_variant_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_variant(m, "y", [&]{
                sdb::append_byte(m, 2);
            });

            m.seal(0);
            BOOST_TEST(m.signature() == "v");

            sdb::read_variant(m, "y", [&]{
                auto b = sdb::read_byte(m);

                BOOST_TEST(b == 2);
            });
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_dict_entry_basic_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_array(m, "{yy}", [&]{
                m.append("{yy}", 1, 2);
            });

            m.seal(0);
            BOOST_TEST(m.signature() == "a{yy}");

            sdb::read_array(m, "{yy}", [&] {
                sdb::read_dict_entry(m, "yy", [&]{
                    auto b1 = sdb::read_byte(m);
                    auto b2 = sdb::read_byte(m);

                    BOOST_TEST(b1 == 1);
                    BOOST_TEST(b2 == 2);
                });
            });
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_dict_entry_complex_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_array(m, "{y(yy)}", [&]{
                m.append("{y(yy)}", 1, 2, 3);
            });

            m.seal(0);
            BOOST_TEST(m.signature() == "a{y(yy)}");

            struct entry {
                int k {};
                int v1 {};
                int v2 {};
            };

            std::vector<entry> dict;

            sdb::read_array(m, "{y(yy)}", [&]{
                int k {}, v1 {}, v2 {};

                while (!m.at_end()) {
                    sdb::read_dict_entry(m, "y(yy)", [&]{
                        k = sdb::read_byte(m);
                        sdb::read_struct(m, "yy", [&]{
                            v1 = sdb::read_byte(m);
                            v2 = sdb::read_byte(m);
                        });
                    });

                    dict.emplace_back(k, v1, v2);
                }
            });

            BOOST_REQUIRE(dict.size() == 1);
            BOOST_TEST(dict[0].k == 1);
            BOOST_TEST(dict[0].v1 == 2);
            BOOST_TEST(dict[0].v2 == 3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(read_dictionary_complex_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_array(m, "{y(yy)}", [&]{
                m.append("{y(yy)}", 1, 2, 3);
            });

            m.seal(0);
            BOOST_TEST(m.signature() == "a{y(yy)}");

            struct entry {
                int k {};
                int v1 {};
                int v2 {};
            };

            std::vector<entry> dict;

            sdb::read_array(m, "{y(yy)}", [&]{
                while (!m.at_end()) {
                    sdb::read_dict_entry(m, "y(yy)", [&]{
                        int k {}, v1 {}, v2 {};

                        k = sdb::read_byte(m);
                        sdb::read_struct(m, "yy", [&]{
                            v1 = sdb::read_byte(m);
                            v2 = sdb::read_byte(m);
                        });

                        dict.emplace_back(k, v1, v2);
                    });
                }
            });

            BOOST_REQUIRE(dict.size() == 1);
            BOOST_TEST(dict[0].k == 1);
            BOOST_TEST(dict[0].v1 == 2);
            BOOST_TEST(dict[0].v2 == 3);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(append_dict_entry_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            sdb::append_array(m, "{yy}", [&]{
                sdb::append_dict_entry(m, "yy", [&]{
                    sdb::append_byte(m, 1);
                    sdb::append_byte(m, 2);
                });
            });

            m.seal(0);
            BOOST_TEST(m.signature() == "a{yy}");

            sdb::read_array(m, "{yy}", [&]{
                sdb::read_dict_entry(m, "yy", [&]{
                    auto k = sdb::read_byte(m);
                    auto v = sdb::read_byte(m);

                    BOOST_TEST(k == 1);
                    BOOST_TEST(v == 2);
                });
            });
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

BOOST_AUTO_TEST_SUITE_END() // value_

} // namespace testing
