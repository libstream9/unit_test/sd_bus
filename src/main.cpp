#define BOOST_TEST_MODULE main
#include <boost/test/unit_test.hpp>

#include "namespace.hpp"

#include <stream9/sd_bus/sdbus.hpp>
#include <stream9/sd_bus/bus.hpp>
#include <stream9/sd_bus/message.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(sd_bus_)

    BOOST_AUTO_TEST_CASE(default_bus_)
    {
        auto b = sdb::default_bus();
    }

    BOOST_AUTO_TEST_CASE(default_system_bus_)
    {
        auto b = sdb::default_system_bus();
    }

    BOOST_AUTO_TEST_CASE(default_user_bus_)
    {
        auto b = sdb::default_user_bus();
    }

    BOOST_AUTO_TEST_CASE(open_bus_)
    {
        auto b = sdb::open_bus("foo");

        BOOST_TEST(b.description() == "foo");
    }

    BOOST_AUTO_TEST_CASE(open_user_bus_)
    {
        auto b = sdb::open_user_bus();
    }

    BOOST_AUTO_TEST_CASE(open_system_bus_)
    {
        auto b = sdb::open_system_bus("foo");

        BOOST_TEST(b.description() == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // sd_bus_

} // namespace testing
