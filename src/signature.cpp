#include <stream9/sd_bus/signature.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(signature_)

    BOOST_AUTO_TEST_SUITE(is_valid_signature_)

        BOOST_AUTO_TEST_CASE(basic_types_)
        {
            static_assert(sdb::is_valid_signature("ybnqiuxtdsogh"));
        }

        BOOST_AUTO_TEST_CASE(variant_)
        {
            static_assert(sdb::is_valid_signature("vvv"));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            static_assert(sdb::is_valid_signature("ay"));

            static_assert(!sdb::is_valid_signature("aa"));
        }

        BOOST_AUTO_TEST_CASE(struct_)
        {
            static_assert(sdb::is_valid_signature("(yy)"));
            static_assert(sdb::is_valid_signature("(y(ybn))"));

            static_assert(!sdb::is_valid_signature("()"));
        }

        BOOST_AUTO_TEST_CASE(dictionary_)
        {
            static_assert(sdb::is_valid_signature("a{sd}"));

            static_assert(!sdb::is_valid_signature("{sd}"));
            static_assert(sdb::is_valid_array_contents_signature("{sd}"));
        }

        BOOST_AUTO_TEST_CASE(complex_)
        {
            static_assert(sdb::is_valid_signature("(sa{i(oii)})ua{sa{ii}}"));
        }

    BOOST_AUTO_TEST_SUITE_END() // is_valid_signature_

    BOOST_AUTO_TEST_CASE(valid_)
    {
        sdb::signature s { "iii" };
    }

#if 0
    BOOST_AUTO_TEST_CASE(invalid_)
    {
        sdb::signature s { "(s" };
    }
#endif

BOOST_AUTO_TEST_SUITE_END() // signature_

} // namespace testing
