#include <stream9/sd_bus/bus.hpp>

#include "namespace.hpp"

#include <stream9/sd_bus/error.hpp>
#include <stream9/sd_bus/message.hpp>
#include <stream9/sd_bus/sdbus.hpp>

#include <stream9/sd_event.hpp>

#include <chrono>

#include <boost/test/unit_test.hpp>

namespace testing {

constexpr auto per_element = boost::test_tools::per_element();

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(bus_)

    //TODO constructors

    BOOST_AUTO_TEST_CASE(anonymous_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(!b.is_anonymous());

        //TODO
        //b.set_anonymous(true);
        //BOOST_TEST(b.is_anonymous());
    }

    BOOST_AUTO_TEST_CASE(trusted_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(b.is_trusted());

        //TODO
        //b.set_trusted(true);
        //BOOST_TEST(b.is_trusted());
    }

    BOOST_AUTO_TEST_CASE(client_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(b.is_client());
    }

    BOOST_AUTO_TEST_CASE(server_)
    {
        auto b = sdb::default_system_bus();

        BOOST_TEST(!b.is_server());
    }

    BOOST_AUTO_TEST_CASE(monitor_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(!b.is_monitor());
    }

    BOOST_AUTO_TEST_CASE(allow_interactive_authorization_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(!b.allow_interactive_authorization());

        b.set_allow_interactive_authorization(true);
        BOOST_TEST(b.allow_interactive_authorization());
    }

    BOOST_AUTO_TEST_CASE(signal_connected_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(!b.signal_connected());
    }

    BOOST_AUTO_TEST_CASE(exit_on_disconnect_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(!b.exit_on_disconnect());
    }

    BOOST_AUTO_TEST_CASE(watch_bind_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(!b.watch_bind());
    }

    BOOST_AUTO_TEST_CASE(tid_)
    {
        auto b = sdb::default_system_bus();

        BOOST_TEST(b.tid());
    }

    BOOST_AUTO_TEST_CASE(bus_id_)
    {
        //TODO
    }

    BOOST_AUTO_TEST_CASE(method_call_timeout_)
    {
        auto b = sdb::default_user_bus();

        BOOST_CHECK(b.method_call_timeout() == 25s);
    }

    BOOST_AUTO_TEST_CASE(creds_mask_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(b.creds_mask() != 0);
    }

    BOOST_AUTO_TEST_CASE(n_queued_read_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(b.n_queued_read() == 0);
    }

    BOOST_AUTO_TEST_CASE(n_queued_write_)
    {
        auto b = sdb::default_user_bus();

        BOOST_TEST(b.n_queued_write() == 1);
    }

    BOOST_AUTO_TEST_CASE(description_)
    {
#if 0
        try {
            auto b = sdb::default_user_bus();

            BOOST_TEST(b.description() == "user"); //TODO libsystemd wrongly return -ENXIO
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
#endif
    }

    BOOST_AUTO_TEST_CASE(scope_)
    {
        auto b = sdb::default_system_bus();

        auto o_rv = b.scope();

        BOOST_REQUIRE(o_rv);
        BOOST_TEST(*o_rv == "system");
    }

    BOOST_AUTO_TEST_CASE(unique_name_)
    {
        auto b = sdb::default_system_bus();

        BOOST_TEST(b.unique_name());
    }

    BOOST_AUTO_TEST_CASE(address_)
    {
        auto b1 = sdb::default_user_bus();
        auto o_rv1 = b1.address();

        BOOST_REQUIRE(o_rv1);
        BOOST_TEST(*o_rv1 == "unix:path=/run/user/1000/bus");

        auto b2 = sdb::default_system_bus();
        auto o_rv2 = b2.address();

        BOOST_REQUIRE(o_rv2);
        BOOST_TEST(*o_rv2 == "unix:path=/run/dbus/system_bus_socket");
    }

    BOOST_AUTO_TEST_CASE(default_sender_)
    {
        //TODO
#if 0
        auto b = sdb::default_user_bus();

        BOOST_TEST(b.default_sender() == "");
#endif
        sdb::bus b { "unix:path=/run/user/1000/bus" };
        b.set_default_sender("org.freedesktop.DBus.Hello");
    }

BOOST_AUTO_TEST_SUITE_END() // bus_

} // namespace testing
