#include <stream9/sd_bus/sdbus.hpp>

#include "namespace.hpp"

#include <stream9/sd_bus/awaiter.hpp>
#include <stream9/sd_bus/bus.hpp>
#include <stream9/sd_bus/message.hpp>
#include <stream9/sd_bus/message_ext.hpp>

#include <stream9/sd_event.hpp>

#include <coroutine>
#include <iostream>

#include <stream9/coroutine/task.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace co = stream9::coroutine;

BOOST_AUTO_TEST_SUITE(async_coro_)

    co::task<void> coro1(auto& ev)
    {
        try {
            auto b = sdb::default_system_bus();

            sdb::method_call m { b,
                "org.freedesktop.systemd1",
                "/org/freedesktop/systemd1",
                "org.freedesktop.systemd1.Manager",
                "GetUnitByPID"
            };

            m.append("u", (uint32_t)getpid());

            auto r = co_await sdb::call_method_async(b, m);

            auto ans = sdb::read_object_path(r);
            std::string_view expected = "/org/freedesktop/systemd1/unit/user_401000_2eservice";

            BOOST_TEST(ans == expected);

            ev.exit(0);
        }
        catch (...) {
            stream9::errors::print_error();
            ev.exit(0);
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(call_method_async_1_)
    {
        try {
            auto b = sdb::default_system_bus();
            auto ev = evt::default_event();

            b.attach_event(ev);

            auto c1 = coro1(ev);
            auto s1 = ev.add_defer([&](auto) {
                c1.resume();
                return 0;
            });

            ev.loop();
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    co::task<void> coro2(auto& ev)
    {
        try {
            auto b = sdb::default_system_bus();

            auto r = co_await sdb::call_method_async(b,
                "org.freedesktop.systemd1",
                "/org/freedesktop/systemd1",
                "org.freedesktop.systemd1.Manager",
                "GetUnitByPID",
                "u", (uint32_t)getpid()
            );

            auto ans = sdb::read_object_path(r);
            std::string_view expected = "/org/freedesktop/systemd1/unit/user_401000_2eservice";

            BOOST_TEST(ans == expected);

            ev.exit(0);
        }
        catch (...) {
            stream9::errors::print_error();
            ev.exit(0);
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(call_method_async_2_)
    {
        try {
            auto b = sdb::default_system_bus();
            auto ev = evt::default_event();

            b.attach_event(ev);

            auto c1 = coro2(ev);
            auto s1 = ev.add_defer([&](auto) {
                c1.resume();
                return 0;
            });

            ev.loop();
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

BOOST_AUTO_TEST_SUITE_END() // async_coro_

} // namespace testing
