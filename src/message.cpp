#include <stream9/sd_bus/message.hpp>
#include <stream9/sd_bus/json.hpp>

#include "namespace.hpp"

#include <stream9/sd_bus/bus.hpp>

#include <stdio.h>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(message_)

    BOOST_AUTO_TEST_CASE(constructor_)
    {
        auto bus = sdb::default_system_bus();

        sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

        auto i = 100;
        m.append_basic('i', &i);

        BOOST_TEST(m.signature(false) == "i");
    }

    BOOST_AUTO_TEST_CASE(append_basic_)
    {
        auto bus = sdb::default_system_bus();
        sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

        auto i = 100;
        m.append_basic('i', &i);

        BOOST_TEST(m.signature(false) == "i");
    }

    BOOST_AUTO_TEST_CASE(array_1_)
    {
        auto bus = sdb::default_system_bus();
        sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

        int arr[2] = { 1, 2, };
        m.append_array('i', arr, sizeof(arr));

        BOOST_TEST(m.signature(false) == "ai");

        m.seal(0);

        try {
            char type {};
            char const* contents {};

            auto has_field = m.peek_type(&type, &contents);
            BOOST_TEST(has_field == true);

            BOOST_TEST(type == 'a');
            BOOST_TEST(contents == "i");

            m.enter_container(type, contents);

            BOOST_TEST(m.signature(false) == "i");

            int i {};

            m.read_basic('i', &i);
            BOOST_TEST(i == 1);
            BOOST_TEST(!m.at_end());

            m.read_basic('i', &i);
            BOOST_TEST(i == 2);
            BOOST_TEST(m.at_end());
            BOOST_TEST(!m.at_end(true));

            m.exit_container();
            BOOST_TEST(m.at_end(true));
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(array_2_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            int arr[2] = { 1, 2, };
            m.append("ai", sizeof(arr) / sizeof(int), arr[0], arr[1]);

            BOOST_TEST(m.signature(false) == "ai");

            m.seal(0);

            auto o_type = m.peek_type();
            BOOST_REQUIRE(o_type);
            BOOST_TEST(o_type->type == 'a');
            BOOST_TEST(o_type->contents == "i");

            int arr2[2];
            m.read("ai", sizeof(arr) / sizeof(int32_t), arr2 + 0, arr2 + 1);
            BOOST_TEST(arr2[0] == 1);
            BOOST_TEST(arr2[1] == 2);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(structure_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            m.open_container(SD_BUS_TYPE_STRUCT, "is");

            int32_t i = 100;
            char const* s1 = "foo";

            m.append_basic(SD_BUS_TYPE_INT32, &i);
            m.append_basic(SD_BUS_TYPE_STRING, s1);

            m.close_container();

            m.seal(0);

            BOOST_TEST(m.signature(false) == "(is)");

            char type {};
            char const* contents {};

            auto has_field = m.peek_type(&type, &contents);
            BOOST_TEST(has_field);
            BOOST_TEST(type == SD_BUS_TYPE_STRUCT);
            BOOST_TEST(contents == "is");

            m.enter_container(type, contents);

            has_field = m.peek_type(&type, &contents);
            BOOST_TEST(has_field);
            BOOST_TEST(type == SD_BUS_TYPE_INT32);
            BOOST_TEST(contents == nullptr);

            i = {};
            m.read_basic(type, &i);
            BOOST_TEST(i == 100);

            has_field = m.peek_type(&type, &contents);
            BOOST_TEST(has_field);
            BOOST_TEST(type == SD_BUS_TYPE_STRING);
            BOOST_TEST(contents == nullptr);

            char const* s2 {};
            m.read_basic(type, &s2);
            BOOST_TEST(s2 == "foo");

            has_field = m.peek_type(&type, &contents);
            BOOST_TEST(!has_field);
            BOOST_TEST(type == 0);
            BOOST_TEST(contents == nullptr);

            m.exit_container();
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(structure_2_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            m.append("(is)", 100, "foo");

            m.seal(0);

            BOOST_TEST(m.signature(false) == "(is)");

            auto o_type = m.peek_type();
            BOOST_REQUIRE(o_type);
            BOOST_TEST(o_type->type == SD_BUS_TYPE_STRUCT);
            BOOST_TEST(o_type->contents == "is");

            int32_t i {};
            char const* s {};
            m.read("(is)", &i, &s);

            BOOST_TEST(i == 100);
            BOOST_TEST(s == "foo");
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(variant_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            // writing
            m.open_container(SD_BUS_TYPE_VARIANT, "i");

            int32_t i = 100;

            m.append_basic(SD_BUS_TYPE_INT32, &i);

            m.close_container();

            m.seal(0);

            BOOST_TEST(m.signature(false) == "v");

            // reading
            char type {};
            char const* contents {};

            auto has_field = m.peek_type(&type, &contents);
            BOOST_TEST(has_field);
            BOOST_TEST(type == SD_BUS_TYPE_VARIANT);
            BOOST_TEST(contents == "i");

            m.enter_container(type, contents);

            has_field = m.peek_type(&type, &contents);
            BOOST_TEST(has_field);
            BOOST_TEST(type == SD_BUS_TYPE_INT32);
            BOOST_TEST(contents == nullptr);

            i = {};
            m.read_basic(type, &i);
            BOOST_TEST(i == 100);

            BOOST_TEST(m.at_end(false));
            BOOST_TEST(!m.at_end(true));

            m.exit_container();

            BOOST_TEST(m.at_end(true));
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(dictionary_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            // writing
            m.open_container(SD_BUS_TYPE_ARRAY, "{si}");

            {
                m.open_container(SD_BUS_TYPE_DICT_ENTRY, "si");

                m.append_basic(SD_BUS_TYPE_STRING, "foo");
                int32_t i = 100;
                m.append_basic(SD_BUS_TYPE_INT32, &i);

                m.close_container();
            }
            {
                m.open_container(SD_BUS_TYPE_DICT_ENTRY, "si");

                m.append_basic(SD_BUS_TYPE_STRING, "bar");
                int32_t i = 200;
                m.append_basic(SD_BUS_TYPE_INT32, &i);

                m.close_container();
            }

            m.close_container();

            m.seal(0);

            BOOST_TEST(m.signature(false) == "a{si}");

            // reading
            char type {};
            char const* contents {};

            auto has_field = m.peek_type(&type, &contents);
            BOOST_TEST(has_field);
            BOOST_TEST(type == SD_BUS_TYPE_ARRAY);
            BOOST_TEST(contents == "{si}");

            m.enter_container(type, contents);

            has_field = m.peek_type(&type, &contents);
            BOOST_TEST(has_field);
            BOOST_TEST(type == SD_BUS_TYPE_DICT_ENTRY);
            BOOST_TEST(contents == "si");

            {
                m.enter_container(type, contents);

                char type {};
                char const* contents {};

                has_field = m.peek_type(&type, &contents);
                BOOST_TEST(has_field);
                BOOST_TEST(type == SD_BUS_TYPE_STRING);
                BOOST_TEST(contents == nullptr);

                char* s {};
                m.read_basic(type, &s);
                BOOST_TEST(s == "foo");

                has_field = m.peek_type(&type, &contents);
                BOOST_TEST(has_field);
                BOOST_TEST(type == SD_BUS_TYPE_INT32);
                BOOST_TEST(contents == nullptr);

                int32_t i = 0;
                m.read_basic(type, &i);
                BOOST_TEST(i == 100);

                BOOST_TEST(m.at_end(false));
                BOOST_TEST(!m.at_end(true));

                m.exit_container();
            }
            {
                m.enter_container(type, contents);

                char type {};
                char const* contents {};

                has_field = m.peek_type(&type, &contents);
                BOOST_TEST(has_field);
                BOOST_TEST(type == SD_BUS_TYPE_STRING);
                BOOST_TEST(contents == nullptr);

                char* s {};
                m.read_basic(type, &s);
                BOOST_TEST(s == "bar");

                has_field = m.peek_type(&type, &contents);
                BOOST_TEST(has_field);
                BOOST_TEST(type == SD_BUS_TYPE_INT32);
                BOOST_TEST(contents == nullptr);

                int32_t i = 0;
                m.read_basic(type, &i);
                BOOST_TEST(i == 200);

                BOOST_TEST(m.at_end(false));
                BOOST_TEST(!m.at_end(true));

                m.exit_container();
            }

            BOOST_TEST(m.at_end(false));
            BOOST_TEST(!m.at_end(true));

            m.exit_container();

            BOOST_TEST(m.at_end(true));

            //m.dump(stdout, SD_BUS_MESSAGE_DUMP_WITH_HEADER);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(json_)
    {
        try {
            auto bus = sdb::default_system_bus();
            sdb::message m { bus, SD_BUS_MESSAGE_METHOD_CALL };

            m.append_basic(SD_BUS_TYPE_STRING, "foo");

            int arr[2] = { 1, 2, };
            m.append_array('i', arr, sizeof(arr));

            m.open_container(SD_BUS_TYPE_STRUCT, "sd");

            m.append("sd", "bar", 3.14);

            m.close_container();

            BOOST_TEST(m.signature() == "sai(sd)");

            m.open_container(SD_BUS_TYPE_ARRAY, "{si}");
            {
                m.append("{si}", "key1", 500);
                m.append("{si}", "key2", 1000);
            }
            m.close_container();

            BOOST_TEST(m.signature() == "sai(sd)a{si}");

            m.open_container(SD_BUS_TYPE_VARIANT, "i");
            {
                m.append("i", 200);
            }
            m.close_container();

            BOOST_TEST(m.signature() == "sai(sd)a{si}v");

            m.seal(0);

            using namespace json::literals;
            auto expected = R"({
               "type": "SD_BUS_MESSAGE_METHOD_CALL",
               "signature": "sai(sd)a{si}v",
               "fields": [
                   "foo",
                   [1, 2],
                   ["bar", 3.14],
                   [["key1", 500],
                    ["key2", 1000] ],
                   200
               ],
               "expect_reply": true,
               "auto_start": true,
               "allow_interactive_authorization": false
            })"_js;

            BOOST_TEST(json::value_from(m) == expected);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

BOOST_AUTO_TEST_SUITE_END() // message_

} // namespace testing
