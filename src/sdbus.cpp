#include <stream9/sd_bus/sdbus.hpp>

#include "namespace.hpp"

#include <stream9/sd_bus/bus.hpp>
#include <stream9/sd_bus/error.hpp>
#include <stream9/sd_bus/json.hpp>
#include <stream9/sd_bus/message.hpp>

#include <stream9/sd_event.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(sdbus_)

    BOOST_AUTO_TEST_CASE(call_method_1)
    {
        try {
            auto bus = sdb::default_system_bus();

            sdb::method_call m {
                bus,
                "org.freedesktop.systemd1",
                "/org/freedesktop/systemd1",
                "org.freedesktop.systemd1.Manager",
                "GetUnitByPID"
            };

            m.append("u", (unsigned)getpid());

            auto r = call_method(bus, m);

            char const* ans = nullptr;
            r.read("o", &ans);

            std::string_view expected = "/org/freedesktop/systemd1/unit/user_401000_2eservice";

            BOOST_TEST(ans == expected);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(call_method_2_)
    {
        try {
            auto bus = sdb::default_system_bus();

            auto msg = call_method(bus,
                "org.freedesktop.systemd1",
                "/org/freedesktop/systemd1",
                "org.freedesktop.systemd1.Manager",
                "GetUnitByPID",
                "u", 0 );

            char const* ans = nullptr;
            msg.read("o", &ans);

            std::string_view expected = "/org/freedesktop/systemd1/unit/user_401000_2eservice";

            BOOST_TEST(ans == expected);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(get_property_)
    {
        auto bus = sdb::default_system_bus();

        auto r = get_property(bus,
            "org.freedesktop.systemd1",
            "/org/freedesktop/systemd1",
            "org.freedesktop.systemd1.Manager",
            "Version",
            "s" );

        char const* ans = nullptr;
        r.read("s", &ans);

        std::string_view expected = "249.2-1-arch"; //TODO this value is volatile. find better value to test

        BOOST_TEST(ans == expected);
    }

#if 0 //TODO NNames is volatile. Write better test
    BOOST_AUTO_TEST_CASE(get_property_trivial_)
    {
        try {
            auto bus = sdb::default_system_bus();

            uint32_t ans {};

            get_property_trivial(bus,
                "org.freedesktop.systemd1",
                "/org/freedesktop/systemd1",
                "org.freedesktop.systemd1.Manager",
                "NNames",
                'u', &ans );

            BOOST_TEST(ans == 306);
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }

    }
#endif

    BOOST_AUTO_TEST_CASE(get_property_string_)
    {
        auto bus = sdb::default_system_bus();

        auto r = sdb::get_property_string(bus,
            "org.freedesktop.systemd1",
            "/org/freedesktop/systemd1",
            "org.freedesktop.systemd1.Manager",
            "Version" );

        std::string_view expected = "249.2-1-arch"; //TODO find other value to test

        BOOST_TEST(r == expected);
    }

    BOOST_AUTO_TEST_CASE(get_property_strv_)
    {
        auto bus = sdb::default_system_bus();

        auto r = sdb::get_property_strv(bus,
            "org.freedesktop.systemd1",
            "/org/freedesktop/systemd1",
            "org.freedesktop.systemd1.Manager",
            "Environment" );

#if 0
        auto expected = {
            "LANG=en_US.UTF-8",
            "PATH=/usr/local/sbin:/usr/local/bin:/usr/bin",
        };

        BOOST_TEST(r == expected, per_element);
#endif
        BOOST_TEST(r[0] == "LANG=en_US.UTF-8");
        BOOST_TEST(r[1] == "PATH=/usr/local/sbin:/usr/local/bin:/usr/bin");
    }

BOOST_AUTO_TEST_SUITE_END() // sdbus_

} // namespace testing
