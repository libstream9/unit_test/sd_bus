#include <stream9/sd_bus/bus.hpp>

#include "namespace.hpp"

#include <stream9/sd_bus/bus_name.hpp>
#include <stream9/sd_bus/error.hpp>
#include <stream9/sd_bus/message.hpp>
#include <stream9/sd_bus/message_ext.hpp>
#include <stream9/sd_bus/object.hpp>
#include <stream9/sd_bus/sdbus.hpp>
#include <stream9/sd_bus/slot.hpp>
#include <stream9/sd_bus/vtable.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/sd_event.hpp>
#include <stream9/strings.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(object_)

    BOOST_AUTO_TEST_CASE(add_object_1_)
    {
        try {
            auto bus = sdb::default_user_bus();

            auto ev = evt::default_event();
            bus.attach_event(ev);

            auto slot = bus.add_object("/org/stream9/test1",
                [](auto m, auto) {
                    sdb::method_return r { m };
                    sdb::append_string(r, "foo");

                    sdb::send(m.bus(), r);

                    return 0;
                });

            sdb::method_call m {
                bus,
                bus.unique_name(),
                "/org/stream9/test1",
                {},
                "Func1"
            };

            auto s2 = sdb::call_method_async(bus, m,
                [&](auto m, auto e) {
                    BOOST_TEST(!e.is_set());

                    auto s = sdb::read_string(m);

                    BOOST_TEST(s == "foo");

                    ev.exit(0);
                    return 0;
                }
            );

            ev.loop();
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

    BOOST_AUTO_TEST_CASE(add_object_2_)
    {
        try {
            auto bus = sdb::default_user_bus();

            auto ev = evt::default_event();
            bus.attach_event(ev);

            sdb::vtable vtbl;
            vtbl.add_method("ToUpper", "s", "s",
                [](auto m, auto) {
                    auto s = sdb::read_string(m);
                    auto upper = str::to_string(str::views::to_upper(s));

                    sdb::method_return r { m };
                    sdb::append_string(r, upper);

                    sdb::send(m.bus(), r);

                    return 0;
                });

            auto obj = bus.add_object(
                "/org/stream9/test1", "org.stream9.test2", std::move(vtbl));

            auto s2 = sdb::call_method_async(bus,
                bus.unique_name(),
                "/org/stream9/test1", "org.stream9.test2", "ToUpper",
                [](auto m, auto) {
                    BOOST_REQUIRE(!m.is_method_error());

                    auto s = sdb::read_string(m);
                    BOOST_TEST(s == "FOO");

                    m.bus().event()->exit(0);
                    return 0;
                },
                "s", "foo"
            );

            ev.loop();
        }
        catch (...) {
            stream9::errors::print_error();
            throw;
        }
    }

BOOST_AUTO_TEST_SUITE_END() // object_

} // namespace testing
