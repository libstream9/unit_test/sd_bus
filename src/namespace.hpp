#ifndef STREAM9_SDBUS_TEST_SRC_NAMESPACE_HPP
#define STREAM9_SDBUS_TEST_SRC_NAMESPACE_HPP

namespace stream9::sd_bus {}
namespace stream9::sd_event {}
namespace stream9::strings {}
namespace stream9::json {}

namespace testing {

namespace evt { using namespace stream9::sd_event; }
namespace sdb { using namespace stream9::sd_bus; }
namespace str { using namespace stream9::strings; }
namespace json { using namespace stream9::json; }

} // namespace testing

#endif // STREAM9_SDBUS_TEST_SRC_NAMESPACE_HPP
